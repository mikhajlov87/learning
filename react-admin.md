# React Admin

Перед работой необходимо _полностью_ и вдумчиво прочитать (Documentation)[https://marmelab.com/react-admin/Readme.html]. Не просмотреть, а прочитать.

Пример как струтурировать проект и код (React admin demo repository)[https://github.com/marmelab/react-admin/tree/master/examples/demo/src]

Сложные связи (много-ко-многим)[https://marmelab.com/ra-enterprise/modules/ra-relationships] можно решить только через платную версию :(
